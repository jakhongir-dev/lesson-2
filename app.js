//// Task 1
const isDivisible3n5 = function (num) {
  const arrDivs = [];
  for (let i = 0; i <= num; i++) {
    if (!(i % 3) && !(i % 5)) {
      arrDivs.push(i);
    }
  }
  console.log(arrDivs);
};
isDivisible3n5(200);

//////////////////////////////////////////////////////////////////////////

// Task 2
const isOddOrEven = function (num) {
  const even = num % 2;
  if (even == 0) {
    console.log(`It's even`);
  } else {
    console.log("It's odd");
  }
};
isOddOrEven(31);
//////////////////////////////////////////////////////////////////////////

// Task 3
const arr = [9, 2, 41, 62, 6, 2, 1, 17, 7, 99];
const newArr = [1];

arr.sort((a, b) => a - b); // ascending
// arr.sort((a, b) => b - a); // descending
console.log(arr);
//////////////////////////////////////////////////////////////////////////

// Task 4
const arrOfObjs = [
  { test: ["a", "b", "w", "c", "d"] },
  { test: ["a", "b", "c"] },
  { test: ["a", "d", "q"] },
  { test: ["a", "b", "k", "e", "e", "l"] },
];

// Function for making unique set of array
const uniqify = function (arr) {
  const unique = [];
  for (obj of arr) {
    for (array of Object.values(obj)) {
      array.forEach((letter) =>
        !unique.includes(letter) ? unique.push(letter) : letter
      );
    }
  }
  console.log(unique);
};
uniqify(arrOfObjs);
